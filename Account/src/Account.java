import java.awt.Component;

import javax.swing.JOptionPane;


public class Account {

	private static Component frame;

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		final int Limit = 3;
		String correctusername = "Kamilla";
		String username;
		String Password;
		String correctpassword = "Dayana";
		String[] choices = { "Admin", "Student", "Staff"};
		int countLimit = 0;
		boolean wrongAccountType = true;
		boolean successfulLogin = false;
		
		//Part 2
		while(wrongAccountType)
		{
			String accountType;
			int accType = 0;
			accountType = (String) JOptionPane.showInputDialog(null,"Choose Account Type","Account Menu",JOptionPane.QUESTION_MESSAGE, null, choices,choices[0]);
			if(accountType.equals(choices[1]))
			{
				accType = 1;
			}
			
			switch(accType)
			{
				case 0:
				{
					wrongAccountType = true;
					JOptionPane.showMessageDialog(null,"Please Choose Correct Account Type");
					break;
				}
				case 1:
				{
					countLimit = 0;
					wrongAccountType = false;
					successfulLogin = false;
					//Part 1
					while (!successfulLogin) 
					{
						username = JOptionPane.showInputDialog("Enter Username ",
								JOptionPane.OK_CANCEL_OPTION);
						if (username.equalsIgnoreCase(correctusername)) 
						{
								Password = JOptionPane.showInputDialog("Enter Password",
										JOptionPane.OK_CANCEL_OPTION);
								if (Password.equalsIgnoreCase(correctpassword)) 
								{
									JOptionPane.showConfirmDialog(null, "Welcome "
											+ accountType);
									successfulLogin = true;
								} 
								else 
								{
									// wrong password
									System.out.println("Fail Authentication. You have "
											+ (Limit - countLimit - 1) + "trial(s) left.");
								}
						} 
						else 
						{
							//wrong username
							JOptionPane.showMessageDialog(null, "That username does not exist");
							System.out.println("Fail Authentication. You have "
									+ (Limit - countLimit - 1) + "trial(s) left.");
						}
						
						countLimit++;
						
						if (Limit == countLimit) {
							wrongAccountType = true;
							successfulLogin = true;
							JOptionPane.showMessageDialog(frame, "Contact Administrator.");
						}
					}
					//END OF PART 1
					break;
				}
				default:
				{
					wrongAccountType = true;
					JOptionPane.showMessageDialog(null,"Please Choose Correct Account Type");
					break;
				}
	}

}
	}}
